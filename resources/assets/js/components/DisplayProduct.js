import React, {Component} from 'react';
import axios from 'axios';
import { Link } from 'react-router';
import TableRow from './TableRow';
import MyGlobleSetting from './MyGlobleSetting';

class DisplayProduct extends Component {
    constructor(props) {
        super(props);
        this.state = {value: '', products: ''};
    }

    getData() {
        axios.get(MyGlobleSetting.url + '/api/products')
        .then(response => {
            this.setState({ products: response.data });
        })
        .catch(function (error) {
            console.log(error);
        })
    }

    componentDidMount(){
        this.getData();
    }

    tabRow(){
        console.log("exect");
        if(this.state.products instanceof Array){
            console.log(this.state.products);

            let f_reload = () => this.getData();
            return this.state.products.map(function(object, i) {
                return (
                  <TableRow
                    key = {"tabrow_" + object.id}
                    obj = {object}
                    reloadTable = {f_reload}
                  />
                );
            })
        } else {
            console.log("ñe");
        }
    }


  render(){
    return (
      <div>
        <h1>Productos</h1>


        <div className="row">
          <div className="col-md-10"></div>
          <div className="col-md-2">
            <Link to="/add-item">Crear nuevo</Link>
          </div>
        </div><br />


        <table className="table table-hover">
            <thead>
            <tr>
                <td>ID</td>
                <td>Nombre</td>
                <td>Descripción</td>
                <td width="200px">#</td>
            </tr>
            </thead>
            <tbody>
              {this.tabRow()}
            </tbody>
        </table>
    </div>
    )
  }
}
export default DisplayProduct;